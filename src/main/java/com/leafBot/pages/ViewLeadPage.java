package com.leafBot.pages;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;


import org.openqa.selenium.WebElement;

import com.leafBot.testng.api.base.Annotations;




public class ViewLeadPage extends Annotations {
	
	@Then("Verify First Name (.*)")
	public ViewLeadPage verifyFirstName(String fName) {
		//String text = driver.findElementById("viewLead_firstName_sp")
		//.getText();
		
		WebElement LeadFirstName = locateElement("id","viewLead_firstName_sp");
		String text = getElementText(LeadFirstName);
		
		if(text.equals(fName)) {
			System.out.println("First name matches with input data "+fName);
		}else {
			System.err.println("First name not matches with input data "+fName);
		}
		return this;
	}

	@And("Click Delete Button") 
	public MyLeadsPage  deleteLead() {
		//driver.findElementByLinkText("Delete").click();
		WebElement Deletebtn = locateElement("link","Delete");
		click(Deletebtn);			
		return new MyLeadsPage ();
	}
	
	@And("Click Edit Button") 
	public editLeadPage EditLead() {
		//driver.findElementByXPath("(//a[@class='subMenuButton'])[3]").click();
		WebElement EditleadBtn = locateElement("xpath","(//a[@class='subMenuButton'])[3]");
		click(EditleadBtn);
		return new editLeadPage ();
	}
	
	@Then("Edit is Verified") 
	public  ViewLeadPage verifyEdit() {
		   // String name = driver.findElementById("viewLead_companyName_sp").getText().replaceAll("[^A-Za-z ]", "");
		
		 WebElement companyNameEle = locateElement("id","viewLead_companyName_sp");
		 String name = getElementText(companyNameEle).replaceAll("[^A-Za-z ]", "");
		    System.out.println(name);
		    if(name.equals("HCL Technologies LTD "))
		    {
		    	System.out.println("Company Name updated as : "+name);
		    }
		    else
		    {
		    	System.out.println("Test Case Failed");
		    }
		    
		    return this;
	}
	
	
	
	
	
	
	
}
