package com.leafBot.pages;

import org.openqa.selenium.WebElement;

import com.leafBot.testng.api.base.Annotations;

import cucumber.api.java.en.And;


public class CreateLeadPage extends Annotations {
	
	@And("Type Company Name (.*)")
	public CreateLeadPage typeCompanyName(String cName) {
		//driver.findElementById("createLeadForm_companyName")
		//.sendKeys(cName);
		WebElement eleCompany = locateElement("id", "createLeadForm_companyName");
		click(eleCompany);
		return this;
	}
	
	@And("Type First Name (.*)")
	public CreateLeadPage typeFirstName(String fName) {
		//driver.findElementById("createLeadForm_firstName")
		//.sendKeys(fName);
		WebElement typeFirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(typeFirstName,fName); 
		
		return this;
	}
	
	@And("Type Last Name (.*)")
	public CreateLeadPage typeLastName(String lName) {
		//driver.findElementById("createLeadForm_lastName")
		//.sendKeys(lName);
		WebElement typeLastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(typeLastName,lName); 
		return this;
	}
	
	@And("Click on the Create Lead Button")
	public ViewLeadPage clickCreateLeadButton() {
		//driver.findElementByClassName("smallSubmit")
		//.click();
		WebElement smallSubmit = locateElement("class", "createLeadForm_companyName");
		click(smallSubmit);
		return new ViewLeadPage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
