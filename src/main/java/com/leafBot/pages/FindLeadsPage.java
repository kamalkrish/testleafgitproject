package com.leafBot.pages;

import org.openqa.selenium.WebElement;

import com.leafBot.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;


public class FindLeadsPage extends Annotations {
	
	@And("Enter FirstName for Search as (.*)")
	public FindLeadsPage TypeSearchName(String searchFname) {
		//driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(searchFname);
		
		WebElement eleSearchName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		clearAndType(eleSearchName, searchFname);
		return this;
			}
	
	@And("Click Find Lead Button")
	public FindLeadsPage clickFindLead()  {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		return this;
			}
	
    @And("Click the first record in the result")
	public ViewLeadPage clickLeadNum()  {
		LeadId = driver.findElementByXPath("(//a[@class='linktext'])[4]").getText();
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		return new ViewLeadPage();
			
	}
    
    @And("I search the Deleted Lead")
    public FindLeadsPage TypeSearchLeadId() {
    	//driver.findElementById("//input[@id='ext-gen247']").sendKeys();
    	
    	driver.findElementByXPath("//input[@name='id']").sendKeys(LeadId);    	
    	
		return this;
			}
    
    @Then("Record is Not found")
    public FindLeadsPage VerifyLeadDelete()
    {
    	String TxtNoRcr = "No records to display";
    	try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
    	String TxtNoRcrApp = driver.findElementByClassName("x-paging-info").getText();
    	if (TxtNoRcr.equals(TxtNoRcrApp))
    			{
    		    System.out.println("Record is Deleted Successfully");
    			}else {
    			System.out.println("Record is Not Deleted");	
    			
    			
    }
    	return this;
    		
    }
    

}
