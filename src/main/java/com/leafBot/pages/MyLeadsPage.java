package com.leafBot.pages;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebElement;

import com.leafBot.testng.api.base.Annotations;



public class MyLeadsPage extends Annotations {
	
	@Given("Click Create Leads Link")
	public CreateLeadPage clickCreateLead() {
		//driver.findElementByLinkText("Create Lead")
		//.click();
		WebElement CreateLead = locateElement("text","Create Lead");
		click(CreateLead);
		return new CreateLeadPage();
		
	}
	@And("Click Find Leads Link")
    public  FindLeadsPage clickFindLead() {
		
		//driver.findElementByLinkText("Find Leads").click();
		
		WebElement FindLeads = locateElement("text","Find Leads");
		click(FindLeads);
		
		return new FindLeadsPage();
	}
	
	
	
	


}
